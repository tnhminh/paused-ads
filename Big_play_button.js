try {
var container = document.getElementById('videojs-player_ima-ad-container').firstChild
var play = document.createElement('div') 
play.style = "width: 74px;" +
	"height: 74px;" +
	"box-sizing: border-box;" +
	"border-style: solid;" +
	"border-color: transparent transparent transparent rgb(32, 32, 32);" +
	"border-width: 37px 0px 37px 56px;" +
	"position: absolute; z-index: 11111;" +
	"top: 50%; left: 50%;" +
	"transform: translate(-50%, -50%);" +
	"cursor: pointer; display: none;";
	
play.addEventListener("click", function() {
	play.style.display = 'none';
	this.player.ima.resumeAd();
});
container.appendChild(play);
this.player.ima.addEventListener(google.ima.AdEvent.Type.CLICK, function() {
	this.player.ima.pauseAd();
});
this.player.ima.addEventListener(google.ima.AdEvent.Type.PAUSED, function() {
	play.style.display = 'block';
});
this.player.ima.addEventListener(google.ima.AdEvent.Type.RESUMED, function() {
	play.style.display = 'none';
});
} catch (e) {
	console.log('Error when adding big play button to TVC !!!!');
}
