/**
 * Copyright 2014 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

var Ads = function (videojs, adTagUrl) {

  this.player = videojs;
  this.adTagUrl = adTagUrl;


  // Remove controls from the player on iPad to stop native controls from stealing
  // our click
  var contentPlayer = document.getElementById('content_video_html5_api');
  if ((navigator.userAgent.match(/iPad/i) ||
    navigator.userAgent.match(/Android/i)) &&
    contentPlayer.hasAttribute('controls')) {
    contentPlayer.removeAttribute('controls');
  }

  this.options = {
    id: 'videojs-player'
  };

  this.events = [
    google.ima.AdEvent.Type.ALL_ADS_COMPLETED,
    google.ima.AdEvent.Type.CLICK,
    google.ima.AdEvent.Type.COMPLETE,
    google.ima.AdEvent.Type.FIRST_QUARTILE,
    google.ima.AdEvent.Type.LOADED,
    google.ima.AdEvent.Type.MIDPOINT,
    google.ima.AdEvent.Type.PAUSED,
    google.ima.AdEvent.Type.STARTED,
    google.ima.AdEvent.Type.THIRD_QUARTILE
  ];

  if (typeof (this.player.ima) === 'function') {
    this.player.ima(
      this.options,
      this.bind(this, this.adsManagerLoadedCallback));
  }

  this.init();
  this.bigPlayButton();
};

Ads.prototype.init = function () {
  this.player.ima.initializeAdDisplayContainer();
  this.player.ima.setContent(null, this.adTagUrl, true);
  this.player.ima.requestAds();
  this.player.play();
};

Ads.prototype.bigPlayButton = function () {
  var self = this;
  var container = document.getElementById('videojs-player_ima-ad-container').firstChild
  var play = document.createElement('div')
  play.style = "width: 74px;" +
    "height: 74px;" +
    "box-sizing: border-box;" +
    "border-style: solid;" +
    "border-color: transparent transparent transparent rgb(32, 32, 32);" +
    "border-width: 37px 0px 37px 56px;" +
    "position: absolute; z-index: 11111;" +
    "top: 50%; left: 50%;" +
    "transform: translate(-50%, -50%);" +
    "cursor: pointer; display: none;";

  play.addEventListener("click", function () {
    play.style.display = 'none';
    self.player.ima.resumeAd();
  });
  container.appendChild(play);
  self.player.ima.addEventListener(google.ima.AdEvent.Type.CLICK, function () {
    console.log('bigPlay click')
    self.player.ima.pauseAd();
  });
  self.player.ima.addEventListener(google.ima.AdEvent.Type.PAUSED, function () {
    console.log('bigPlay pause')
    play.style.display = 'block';
  });
  self.player.ima.addEventListener(google.ima.AdEvent.Type.RESUMED, function () {
    console.log('bigPlay resume')
    play.style.display = 'none';
  });
};

Ads.prototype.adsManagerLoadedCallback = function () {
  for (var index = 0; index < this.events.length; index++) {
    this.player.ima.addEventListener(
      this.events[index],
      this.bind(this, this.onAdEvent));
  }
  this.player.ima.startFromReadyCallback();
};

Ads.prototype.onAdEvent = function (event) {
  this.log('Ad event: ' + event.type);
};

Ads.prototype.log = function (message) {
  console.log(message);
}

Ads.prototype.bind = function (thisObj, fn) {
  return function () {
    fn.apply(thisObj, arguments);
  };
};