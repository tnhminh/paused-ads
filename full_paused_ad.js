var pauseFunc = {
    configAd: {},
    g: '',
    api: function() {
        return document.location.protocol + "//log.adsplay." + (pauseFunc.envDev() ? "xyz" : "net") + "/track/ads"
    },
    envDev: function() { return document.location.hostname.indexOf("dev") >= 0 }, 
    apiDelivery: function (e) {
        for (var t = (new Date).getTime(), n = pauseFunc.d(), o = "uuid=" + pauseFunc.r(), a = 0; a < e.length; a++)
            o += "&placement=" + e[a];
        o += "&referrer=" + encodeURIComponent(document.referrer ? document.referrer : ""),
            o += "&url=" + encodeURIComponent(document.location.href),
            o += "&cxt=" + encodeURIComponent(pauseFunc.s()),
            o += "&cxkw=" + encodeURIComponent(n.ckw),
            o += "&ut=" + encodeURIComponent(n.ut),
            o += "&cid=" + encodeURIComponent(n.cid),
            o += "&loc=" + encodeURIComponent(pauseFunc.c()),
            o += "&t=" + t;
        var i = "d4"
            , l = t % 8
            , u;
        if (1 === l ? i = "d1" : 3 === l ? i = "d3" : 5 === l ? i = "d5" : 6 === l && (i = "d6"),
            pauseFunc.envDev())
            u = document.location.protocol + "//" + i + ".adsplay.net/delivery";
        else
            u = "https:" + "//" + "d" + ".adsplay.net/delivery";
        return u + "?" + o
    },
    r: function () {
        var e = AdsPlayCookies.get("apluuid");
        return e || (e = i(),
            AdsPlayCookies.set("apluuid", e, {
                expires: 315569520
            })),
            e
    },
    d: function () {
        for (var e = 0, t = "", n = "", o = document.getElementsByTagName("meta"), a = 0; a < o.length; a++) {
            var i = o[a];
            i.getAttribute && ("user" === i.getAttribute("name") && (e = i.getAttribute("content")),
                "category" === i.getAttribute("name") && "" === t && (t = i.getAttribute("content")),
                "contentid" === i.getAttribute("name") && "" === n && (n = i.getAttribute("content")),
                "og:image" === i.getAttribute("name") && "" === t && (t = i.getAttribute("content")))
        }
        return {
            cid: n,
            ut: e,
            ckw: t.substr(0, 100)
        }
    },
    s: function () {
        var e = ""
            , t = document.getElementById("livetv_title");
        return t || (t = document.getElementById("vod_url")),
            e = t ? t.innerHTML : document.title,
            e = e.substr(0, 100)
    },
    c: function () {
        var e = AdsPlayCookies.get("aplloc");
        if ("string" != typeof e) {
            var t = "https://static.fptplay.net/zone.html?time" + (new Date).getTime();
            AdsPlayCorsRequest.get(!1, t, ["x-zone", "content-type"], function (t, n) {
                e = "vn-" + t["x-zone"],
                    AdsPlayCookies.set("aplloc", e, {
                        expires: 604800
                    })
            }),
                e = ""
        }
        return e
    },
    sendLogTracking: function (e) {
        (e = e || {}).url = e.url || null,
            e.vars = e.vars || {},
            e.error = e.error || function () { }
            ,
            e.success = e.success || function () { }
            ,
            e.vars.cb = Math.floor(1e13 * Math.random());
        var t = [];
        for (var n in e.vars)
            t.push(encodeURIComponent(n) + "=" + encodeURIComponent(e.vars[n]));
        var o = t.join("&");
        if (e.url) {
            var a = new Image;
            a.onerror && (a.onerror = e.error),
                a.onload && (a.onload = e.success),
                e.noParameter ? a.src = e.url : a.src = e.url + "?" + o
        }
    },
    trackClick: function (e) {
        hideAds();
        pauseFunc.sendLogTracking({
            url: pauseFunc.api(),
            vars: {
                metric: "click",
                adid: e,
                beacon: pauseFunc.configAd.adBeacon
            }
        })
    }
    ,
    closeOverlayAd: function (e) {
        hideAds();
        pauseFunc.sendLogTracking({
            url: pauseFunc.api(),
            vars: {
                metric: "close",
                adid: e,
                beacon: pauseFunc.configAd.adBeacon
            }
        })
    }
}

var showAds = function () {    
    if (!player.paused() || player.isFullscreen_ || (typeof player.ima !== 'undefined' && player.ima.adPlaying)) {
        console.log('Paused ad: something is playing ...', !player.paused(), typeof player.ima !== 'undefined' && player.ima.adPlaying)
        return;
    }
    console.log('Paused ad: showAds ...');
    pausedAdShowing = true;
    var configAd = pauseFunc.configAd,
        cTime = Date.now(),
        videoElement = document.getElementById(configAd.videoPlayerId)
        adId = configAd.adId
        iconClose = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAkCAYAAADhAJiYAAAABHNCSVQICAgIfAhkiAAAAuBJREFUWIW9mEtPFEEUhb8zoCaIKxMZBgZcCFH/gYk7Q0CBBCQsWMEKov4bE6MbNyb+BJeKETBhA8hjEHZqXBh1wSM8JJl2MTXSNP24xQyepJNOuqru6XNvn7rVwg8CbgA9oDvAbaBTqBnIQbAVwDegBMEsMA1sAoFnnEw0AsOgadCeyAWWC7QPeg+MuDVqhqiosWAlkUJuBeh1a54JTaBnoD+1kgmROgK9AC77ksmDPohcuV5kIsQ+Aq1WMi2gxfMgEpPCvCVNs+dNJkRqDmgOE2gI3Qv0VGiYGgrPB0JF4CrwJu55Tz0L2K/Q6YuSaQR9MkzeBaZAq4axJWAStGOsp4thQg8Nk/aBITe+CFpLGfsZ6HRjBy1mCowepxK9NSgzxEm0g1YSlOmIjO0HbWfEmMHVbjdoP4P9VEJdtoeVcspEyVQxoRRfcxnoBpgy5riYEKhYea5SKE1RFCzeBjwC9Mr4NawBbQkB2ypqxaIVtGSM8RqfjRO0nBI4Dnkf1wctIPTDOsFNWjWSMitzfOkXInfoN+mfUoUUMtfO2LIc5oCyRwqquMDJbSeKBjfmLNBPT3U2gOuGhVPNMyFlv/Es6rRPOw5toGWP9RdzQMmyckCwCcF94EvM4xbiG67vEDwICNaML7AO8KRGZQqgpQxLaLcoBTwG6DJsHZMJgVrDXYKzhCTzHFf61nEA3ITK5jqdodAOMBhD5pTpOaWipPpAWxkx5gg1hiOGtO0B/aE0JfZPEfPsy+qJQGVgLPwGjbbNT9vAhMWBnVLjWcqEavRSNMe9xhbW52iUOda1sANxRSfQc8+ANV+gl+HaiaIJNPOfyJRB88CVJDJVFCxNfB3IrJNsEaeQB82dY5rmfchU0Vz5MaCjOhI5cjWTmaYkiIqPnDpdeBIpu097gDqdii8Co5WjSvo2EyFy4Bx4jBifSVLABwK6gHugu8AtQQfIpSDYDeArsO5+6b0DNvD4pfcXqZsQX5rRlEIAAAAASUVORK5CYII=";
    if (videoElement.firstChild) {
        playerWidth = player.currentWidth();
        playerHeight = player.currentHeight();
        timeleft = Math.ceil(configAd.autoHideTimeout / 1000)
        adHeight = (playerHeight * adHeightPercent) / 100 + "px;";
        fromTop = (playerHeight * ((100 - adHeightPercent) / 2)) / 100 + "px;";
        var trackClick = '<a onclick="pauseFunc.trackClick(' + adId + ')" href="' + configAd.clickthroughUrl + '" title="' + configAd.clickActionText + '" target="_blank" >' + ('<img style="height: 100%;"src="' + configAd.adMedia + '" />') + "</a>"
            , closeAd = '<a onclick="pauseFunc.closeOverlayAd(' + adId + ')" href="javascript:" style="position: absolute; width:25px; height:25px; top:-5px; right:-5px; background-image: url(' + iconClose + ');background-size: contain;background-repeat: no-repeat;z-index: 99;"  ></a>'
            , pauseAdContainer = document.getElementById("div-ad-paused-" + adId + "-" + currentTime)
            , background = '<div style="position: absolute; width: 100%; height: 100%; background-color: #fff; opacity: 0.6">'
            , counter = '<div id="counter" style="position: absolute;right: 30px;top: 30px;font-size: 36px;width: 50px;height: 50px;border-radius: 50%;box-shadow: 0 0 0 3pt;">' + timeleft + '</div>';
        if (pauseAdContainer == null) {
            pauseAdContainer = document.createElement("div"),
                pauseAdContainer.id = "div-ad-paused-" + adId + "-" + cTime,
                pauseAdContainer.style.position = "absolute",
                pauseAdContainer.style["text-align"] = "center",
                pauseAdContainer.style["background-color"] = "rgba(221, 221, 221, 0)",
                pauseAdContainer.style.width = playerWidth + "px",
                pauseAdContainer.style.height = playerHeight + "px",
                //pauseAdContainer.style.top = videoElement.offsetTop + videoElement.offsetHeight - 130 + "px",
                pauseAdContainer.style["z-index"] = "3",
                pauseAdContainer.innerHTML = background + counter +'</div><div style="display: inline-block; position: relative; height: ' + adHeight + '; top: ' + fromTop + ';">' + trackClick + closeAd + "</div>";
            videoElement.insertBefore(pauseAdContainer, videoElement.firstChild)
        } else {
            pauseAdContainer.setAttribute("id", "div-ad-paused-" + adId + "-" + cTime);
            pauseAdContainer.style.display = "block";
            pauseAdContainer.style.width = playerWidth + "px";
            pauseAdContainer.style.height = playerHeight + "px";
            var countElement = document.getElementById("counter");
            if (countElement != null) {
                countElement.innerHTML = timeleft;
            }
        }
        currentTime = cTime;
        pauseFunc.sendLogTracking({
            url: pauseFunc.api(),
            vars: {
                metric: "impression",
                adid: adId,
                beacon: pauseFunc.configAd.adBeacon
            }
        });
        for (var m = 0; m < configAd.tracking3rdUrls.length; m++)
            pauseFunc.sendLogTracking({
                url: configAd.tracking3rdUrls[m],
                vars: {},
                noParameter: !0
            });

        downloadTimer = setInterval(function () {
            var countElement = document.getElementById("counter");
            if (timeleft >= 0) {
                if (timeleft !== 0) {
                    if (countElement != null) {
                        countElement.innerHTML = timeleft - 1;
                    }
                    --timeleft;
                } else {
                    hideAds();
                }
            } else {
                clearInterval(downloadTimer);
            }
        }, 1000);
    }
}

var hideAds = function () {
    var element = document.getElementById("div-ad-paused-" + pauseFunc.configAd.adId + "-" + currentTime);
    if (element != null) {
        element.style.display = "none";
    }
    if (downloadTimer) {
        clearInterval(downloadTimer)
    }
    if (timeleft) {
        timeleft = -1;
    }
    pausedAdShowing = false;
}

var startPausedEvent = function () {
    var configAd = pauseFunc.configAd;
    if (typeof player !== 'undefined' && player) {
        console.log("Paused ad: setting paused ads event ... ");

        if (document.getElementById(configAd.videoPlayerId) && typeof configAd.adMedia === 'string') {
            player.on('pause', function () {
                console.log('Paused ad: player pause ...')
                setTimeout(function () {
                    showAds();
                }, 500);
            })
            player.on('play', function () {
                if (pausedAdShowing) {
                    hideAds();
                }
            });
        } else {
            console.log("Paused ad: data error, stop ... ");
        }
    } else {
        console.log("Paused ad: waiting for player ... ");
        setTimeout(function () {
            startPausedEvent()
        }, 2000);
    }
};


var currentTime = 0,
    pausedAdShowing = false,
    adHeightPercent = 95,
    playerWidth, playerHeight,
    downloadTimer, timeleft;

var getConfigPausedAd = function () {
    for (var e = [401, 501], t = document.getElementsByClassName("adsplay-placement"), t = {}, o = 0; o < t.length; o++) {
        var a = t[o];
        e.push(a.getAttribute("data-placement-id"));
    }
    var checkAdsplay = function (e, t) {
        if (AdsPlay && AdsPlayCorsRequest) {
            getLink(e, t)
        }
        else {
            console.log("Paused ad: waiting for AdsPlayCorsRequest ... ");
            setTimeout(function () {
                checkAdsplay(e, t);
            }, 200);
        }
    }

    var getLink = function (e) {
        var n = pauseFunc.apiDelivery(e) + "&at=overlay&isPausedClick=true";
        AdsPlayCorsRequest.get(!1, n, [], function (e, t) {
        console.log(t)
            //t = '[{"videoPlayerId":"player-wrapper","autoHideTimeout":10000,"timeToShow":180000,"width":480,"height":120,"tracking3rdUrls":[],"adMedia":"//st50.adsplay.net/ads/overlay/1535617377858/f17ef66ad17d682e3023e2df7e23c304.jpg","clickthroughUrl":"https://thammyhanquoc.vn/khuyen-mai-30-04-01-05?utm_source\u003dFPTPlay\u0026utm_medium\u003dGiam30Thang5\u0026utm_campaign\u003dJWFPTPlay","clickActionText":"","adId":2276,"adBeacon":"zgzrzi1yzkzqzm1u20zjzhzmzn20zg1v1v20znzizk1v1tzkzm1yznzjzhzr211y2pznzjzi2pzhzhzkzl2pzj2pzj","adType":3,"placementId":401,"adCode":""},{"videoPlayerId":"player-wrapper","autoHideTimeout":10000,"timeToShow":600000,"width":480,"height":120,"tracking3rdUrls":[],"adMedia":"//st88.adsplay.net/ads/overlay/1535617377858/f17ef66ad17d682e3023e2df7e23c304.jpg","clickthroughUrl":"https://thammyhanquoc.vn/khuyen-mai-30-04-01-05?utm_source\u003dFPTPlay\u0026utm_medium\u003dGiam30Thang5\u0026utm_campaign\u003dJWFPTPlay","clickActionText":"","adId":2276,"adBeacon":"zgzrzi1yzkzqzm1u20zjzhzmzn20zg1v1v20znzizk1v1tzkzm1yznzjzhzr211y2pznzjzi2pzhzhzkzl2pzj2pzj","adType":3,"placementId":401,"adCode":""}]';
            try {
                var result = JSON.parse(t);
                if (result && result[0] && typeof result[0] === 'object') {
                    pauseFunc.configAd = result[0];
                    startPausedEvent();
                }
            } catch (e) {
                console.log('Paused ad: error getting config: ', e)
            }
        })
    }

    checkAdsplay(e, t);
}

getConfigPausedAd();
